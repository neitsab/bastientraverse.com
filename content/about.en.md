---
title: About
created: 2022-06-23T18:52:19+02:00
lang: en
---

## Contact

You are more than welcome to contact me, and can do so by clicking the enveloppe icon [in the footer](#footer) 👇😊

## Technical stack

Built with [Hugo](https://gohugo.io) using a [modified](https://gitlab.com/neitsab/bastientraverse.com) [Even](https://github.com/olOwOlo/hugo-theme-even) theme.

## License

Original content under [CC-BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/ "Creative Commons Attribution-ShareAlike 4.0 International license").
