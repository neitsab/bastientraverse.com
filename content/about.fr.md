---
title: À propos
created: 2022-06-23T18:52:19+02:00
lang: fr
---

## Contact

Vous êtes plus que les bienvenus pour m'envoyer un email, en cliquant sur l'enveloppe [au bas de la page](#footer) 👇😊

## Logiciels employés

Site créé avec le générateur [Hugo](https://gohugo.io) en utilisant une version [modifiée](https://gitlab.com/neitsab/bastientraverse.com) du thème [Even](https://github.com/olOwOlo/hugo-theme-even).

## Licence

Tout le contenu original est sous licence [CC-BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/deed.fr "Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International").
