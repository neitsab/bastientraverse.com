---
title: Cryptocurrenshit
date: 2021-04-27T19:45:26+02:00
lang: en
categories: Tech
tags:
  - tech
  - economy
  - links
---

From [Cryptocurrency is an abject disaster](https://drewdevault.com/2021/04/26/Cryptocurrency-is-a-disaster.html) by Drew DeVault:

> Cryptocurrency is one of the worst inventions of the 21st century. I am ashamed to share an industry with this exploitative grift. It has failed to be a useful currency, invented a new class of internet abuse, further enriched the rich, wasted staggering amounts of electricity, hastened climate change, ruined hundreds of otherwise promising projects, provided a climate for hundreds of scams to flourish, created shortages and price hikes for consumer hardware, and injected perverse incentives into technology everywhere. Fuck cryptocurrency.

Ditto.

Further reading on this topic on [Wikipedia](https://en.wikipedia.org/wiki/Cryptocurrency#Reception), and for an illustration of the abuse it is causing see [this Hacker News thread](https://news.ycombinator.com/item?id=26936932).

**2021-07-15** The creator of Dogecoin shared his view on cryptocurrencies:

> After years of studying it, I believe that cryptocurrency is an inherently right-wing, hyper-capitalistic technology built primarily to amplify the wealth of its proponents through a combination of tax avoidance, diminished regulatory oversight and artificially enforced scarcity.

[Full Twitter thread](https://mobile.twitter.com/ummjackson/status/1415353984617914370) / [HN submission](https://news.ycombinator.com/item?id=27841947).

**2021-10-07** [Bitcoin is a Ponzi scheme](https://ic.unicamp.br/~stolfi/bitcoin/2020-12-31-bitcoin-ponzi.html).

**2021-10-13** Cryptocurrencies expose you to the risk of developing a form of [gambling addiction](https://castlecraig.co.uk/treatment/behavioral-addictions/bitcoin-addiction-treatment) (on a personal note, I had these exact same symptoms the one and only time I got into the cryptocurren-shit show, when trying to convert in fiat Lumens received from a Keybase initiative. Damn did I get a bad couple of hours stressing over all the parameters and trying to maximize profit...).

**2022-01-15** ... And it's not like [it was still crypto early days](https://blog.mollywhite.net/its-not-still-the-early-days/)!

**2022-02-12** A very excellent scholarly lecture: [Can We Mitigate Cryptocurrencies’ Externalities?](https://blog.dshr.org/2022/02/ee380-talk.html)

**2022-02-15** Here is a [Twitter thread](https://twitter.com/thomasg_eth/status/1492663192404779013) where a prominent cryptocurrenshit holder explains how he was victim of a refined scam and very nearly lost all of his millions.

- takeaway n°1: even being super knowledgeable about this space doesn't shield you from those tricky schemes. So what about the average Joes and Janes?
- takeaway n°2: Ethereum-adjacent discourse is straight up alien speak to me. Even though I generally feel rather proficient on the intellectual level, I'm still dumbfounded when I read the kind of mumble-jumble this thread is riped with. And in a very different way than when I'm reading something technical in a field I am not acquainted with, like say particle physics or philology. The closest thing I can compare it with is global finance, which to me says a lot about the [habitus](https://en.wikipedia.org/wiki/Habitus_(sociology)) that governs the field of cryptocurrency...

See also the related [HN thread](https://news.ycombinator.com/item?id=30322715).

**2022-05-15** [All Cryptocurrency Should “Die in a Fire”](https://www.currentaffairs.org/2022/05/why-this-computer-scientist-says-all-cryptocurrency-should-die-in-a-fire/) -- [HN thread](https://news.ycombinator.com/item?id=31376192)

**2022-06-23** [Bitcoin Is A Hideous Monstrosity Made Out Of Computers And Greed That Must Be Destroyed Before It Devours The World](https://medium.com/@michelcryptdamus/cryptocurrency-is-a-hideous-monstrosity-made-out-of-computers-and-greed-that-must-be-destroyed-99c26a1bbbaf) -- [HN thread]](https://news.ycombinator.com/item?id=31833316)

**2022-06-24** Alright that's it, even [Bruce Schneier is against cryptocurrenshit](https://www.schneier.com/blog/archives/2022/06/on-the-dangers-of-cryptocurrencies-and-the-uselessness-of-blockchain.html)!

**2023-02-09** [Yes, Crypto is ALL a Scam](https://www.stephendiehl.com/blog/crypto-is-a-scam.html) -- [HackerNews discussion](https://news.ycombinator.com/item?id=34691425)

**2023-05-30** [Web3 is doing just great](https://web3isgoinggreat.com/) ...and is definitely not an enormous grift that's pouring lighter fluid on our already smoldering planet -- from [HN thread](https://news.ycombinator.com/item?id=36128484)
