---
title: Blogging from a smartphone for tech-savvy people
date: 2020-01-21T18:30:00+07:00
lastmod: 2021-04-15T02:30:00+02:00
categories: Tech
tags: [ "web", "blog", "hugo", "android" ]
---

In this post we will see how to create and publish a blog using a smartphone, all the while flexing some serious tech chops.

<!--more-->

## Requirements:

- a smartphone running Android
- the [Termux](https://termux.com) app with `git` and `hugo` installed (`pkg install git hugo`) and properly [configured](https://wiki.termux.com/wiki/Termux-setup-storage) to access the [local storage](https://wiki.termux.com/wiki/Internal_and_external_storage)
- a [git client](https://play.google.com/store/search?q=git%20client&c=apps) (Play store link) like the older but quite functional [Forker](https://play.google.com/store/apps/details?id=ch.phcoder.jigit) app for easy graphical interactions with git (commit, push...): especially useful on the long run for a smoother publishing experience, but you may also exclusively use the command line for increased hacker-cred
- a text editor: I strongly recommend [MiXplorer](https://mixplorer.com/) (free on [XDA Labs](https://labs.xda-developers.com/store/app/com.mixplorer) or support version in the Play Store), which provides editors suitable for both code and text and is an amazing file explorer all around; [Markor](https://gsantner.net/project/markor.html) is a good second choice
- accounts on compatible git hosting and CDN deployment services (I'll be using [GitLab](https://about.gitlab.com/) and [Vercel](https://vercel.com/)): although the CDN part is optional, it provides some nice benefits and a smoother experience overall than e.g. GitHub/GitLab Pages
- a mobile browser (I recommend [Firefox for Android](https://play.google.com/store/apps/details?id=org.mozilla.firefox)) and of course... Internet access 😁

## Instructions:

- create a new empty project on GitLab and clone it in Termux/Forker (**tip:** use the HTTPS URL in Termux to avoid having to install and setup openssh)
- in Termux, `cd` to the repo folder (`~/storage/shared/Android/data/ch.phcoder.jigit/files/repo/*name*` if using Forker) and run `hugo create site . -f` in it
- follow [Hugo Quick Start Guide](https://gohugo.io/getting-started/quick-start/#step-3-add-a-theme) to add a theme and some content to your site
- check that everything is working as intended by running `hugo server -D` and opening <http://localhost:1313> in your browser
- then commit and push your site to GitLab through Termux/Forker
- now follow the official Vercel guide for [deploying a Hugo site](https://vercel.com/guides/deploying-hugo-with-vercel#step-2:-deploy-your-hugo-website-with-vercel)
- finally, enjoy your new blog created on your smartphone and available worldwide! 👍

## Going further:

- [customize your site](https://gohugo.io/getting-started/configuration/) to suit your needs
- set up a [custom domain](https://vercel.com/docs/custom-domains) and enjoy the automatic HTTPS (really smoothly done by Vercel) to be a good netizen

Now whenever you want to add some content to your blog, just create a new post in the proper folder either using `hugo new posts/post_title` in Termux or directly via your text editor, commit and push your changes and your updated site will be automagically deployed. You are officially equipped to blog on the go while roaming the vast world! Great success✌
