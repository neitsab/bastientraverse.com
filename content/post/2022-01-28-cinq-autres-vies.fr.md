---
title: Cinq autres vies
date: 2022-01-28T21:42:00+01:00
lang: fr
categories: ["Intérieur", "Carnets"]
tags: ["intérieur", "carnets", "réflexions"]
---

En lisant le tome 1 de [Émotions : enquête et mode d'emploi](https://www.pourpenser.fr/emotions-enquete-et-mode-d-emploi/340-emotions-enquete-et-mode-d-emploi-tome1-2019.html)...

« Si tu pouvais vivre cinq autres vies[^0], en plus de celle-ci, qu'est-ce que tu ferais ? »

1. ...
2. ...
3. ...
4. ...
5. ...

En fait, ce n'est pas leur contenu qui m'intéresse tant, pas d'y avoir telle ou telle occupation, mais plutôt le *comment* je les vivrais, dans quel état mental et émotionnel.

J'aimerais que toutes mes vies soient libres de souffrance, mais peut-on vivre sans souffrir ? Apprendre sans jamais souffrir ?

J'aimerais qu'elles soient harmonieuses, paisibles, épanouissantes. De vivre entouré de gens que j'apprécie et qui m'apprécient également, que l'on sache communiquer sainement ensemble et avec nous-même.

J'aimerais me rendre utile aux autres, travailler pour la communauté sans m'épuiser mais en demeurant en progression constante.

J'aimerais être une personne équilibrée, fonctionnelle : savoir comment agir avec moi-même et avec les autres, et avoir les ressources personnelles pour soutenir mon développement et celui de mes semblables.

J'aimerais vivre en harmonie avec mon milieu, avec la nature, vivre dans une culture qui sache prendre soin de la Terre. Que l'on soit libéré de l'avidité, que l'on puisse vivre adroitement, en préservant les équilibres.

Mais est-il possible de vivre en échappant au vieillissement, à la maladie, à la séparation, à la mort ? Est-il possible de naître sage et de demeurer libre de l'influence de l'envie, de la colère, de la peur ?

Je ne le crois pas, alors je me contenterai de continuer à pratiquer dans cette vie le développement des vertus et l'éradication des vices, d'entreprendre le triple entraînement en moralité, concentration et discernement. Je continuerai de tenter de me perfectionner en tant qu'être humain, en m'attachant le moins possible aux conditions extérieures du moment que celles-ci sont favorables au but que je me suis fixé.

[^0]: En supposant qu'on parle uniquement de vies humaines, dans le cadre terrestre que l'on connaît.
