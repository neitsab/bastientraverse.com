---
title: Retourner dans le monde
date: 2022-07-10T00:40:00+02:00
lang: fr
categories: ["Intérieur", "Carnets"]
tags: ["intérieur", "carnets"]
---

Ça y est, me voilà à l'aube d'un grand changement. C'est à la fois tout simple, et en même temps hors norme. La période que je laisse derrière moi, toutes ses difficultés et la solitude désespérante... Je me sens un peu comme juste avant mon arrivée à l'ashram en 2018. Un saut dans l'inconnu, en confiance.

Alors que, franchement, il y a des chances pour que ce soit tout à fait *anticlimactic*/décevant. Ce sont des conditions que je connais, rien de très engageant... Mais c'est de renouer avec ce monde et ma voie, après plus de deux ans d'interruption et tout ce qui a jalonné (pour ne pas dire *jonché*) cette période, qui me fait sentir à nouveau souffler en moi le vent chaud de la vie en mouvement.

Je reprends ma place, je retourne dans le monde. Intention accomplie.
