---
title: Refaire le passé
date: 2020-12-13T02:08:40+01:00
#lastmod: 2021-04-15T01:30:40+02:00
lang: fr
categories: ["Réflexions", "Intérieur"]
tags: ["réflexions", "intérieur"]
---

Il y a quelques années, j'ai réalisé que le passé n'était pas si figé que ce que l'on pouvait croire.

<!--more-->

Vous savez, on dit toujours : "On ne peut pas refaire le passé". Mais qu'est-ce que le passé ?

C'est un souvenir, une trace, un récit.

Et un souvenir, une trace, un récit, ça se **revisite**, ça s'altère. Non pas dans les faits, mais dans leur subsistance et leurs effets dans notre mémoire. On peut transformer la trace, même si l'on ne peut pas changer ce qui l'a causée.

C'est comme cela que j'ai découvert qu'on pouvait changer son passé. En changeant son influence sur nous aujourd'hui.
