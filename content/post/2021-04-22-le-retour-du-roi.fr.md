---
title: Le Retour du Roi
date: 2021-04-22
categories: Conscience
tags: ["conscience", "chamanisme", "bouddhisme", "nature"]
---

{{< youtube N9lnCqOhVxk >}}

***

Ça m'a fait tellement plaisir quand j'ai vu ça. C'est comme un grand cadeau. Toute cette énergie partagée, la droiture intérieure, la positivité, l'amour, la lumière, la force... Bien évidemment.

Pour moi c'est un événement qui marque l'arrivée d'un nouveau cycle, d'une nouvelle période. Que ça se produise maintenant est signifiant.

Sans exagérer, ça me fait un peu le même effet que si je voyais la roue de la Loi être mise en mouvement devant moi : c'est une promesse que les choses vont aller vers le mieux, que l'on va recevoir du soutien. C'est comme un ami que l'on n'aurait pas vu depuis longtemps et qui réapparaîtrait soudainement pour nous donner d'excellentes nouvelles, ou comme d'être connecté à une source à 200 000 volts... C'est tout l'effet que ça me fait !

Hommage à la forêt, et merci Laurent !
