---
title: Data transmission
date: 2015-07-29T01:44:00+02:00
updated: 2022-03-11
published: 2022-03-11
lang: en
categories: Tech
tags: ["tech", "science", "learning"]
#slug: data-transmission
---

Everything "digital", everything related to computers is based on signal and quantization theories.

Today I was considering the following question: "How do electricity and light convey information?". It seemed incomprehensible to me that we could vehicle structured information via those raw streams of particles. How is it possible to give electrons a specific meaning, like to register this text I am writing and send it over various types of wire (copper and optic fiber)?

I picture the phone line getting out of my house and my home modem-router plugged in it. *Everything* Internet related goes through this one copper wire. How is it possible that TV shows, distro images and other emails and web pages are transported by mere unformed electrons? This all seems really mysterious to me, and piqued my interest tonight.

So after searching a bit I ended up on Wikipedia's [data transmission](https://en.wikipedia.org/wiki/Data_transmission) article via the marvelous process of serendipity, and all my answers started getting answered.

(To be fair, I got a quick and useful answer from [this site](http://www.tigoe.com/pcomp/code/circuits/understanding-electricity/) which gathers a few key concepts about electricity and drops in its intro the core of our question).

So as a matter of fact and to cut the suspense out, information can be coded into electricity and light not by modifying the inner particles but by shaping variations of their flow and parsing those on the receiving end.

In other words, *standardized operations are applied to transported streams that make up the encoding*.

And for the full blown explanation: [physical layer](https://en.wikipedia.org/wiki/Physical_layer) (OSI model), Wikipedia.

**Addendum (2022-03-11):** There is a wonderful book about this exact topic that often gets recommended on Hacker News, [Code: The Hidden Language of Computer Hardware and Software](https://en.wikipedia.org/wiki/Code%3A_The_Hidden_Language_of_Computer_Hardware_and_Software) by Charles Petzold. It starts off from the predecessors of digital signal, Morse code, Braille and other ways of encoding information through normalized signals, and then retraces the evolution from there all the way to modern computing. Truly a didactic masterpiece interspersed with many other helpful experiments/practical bits.
