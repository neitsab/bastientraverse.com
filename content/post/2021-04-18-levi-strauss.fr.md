---
title: Claude Lévi-Strauss
date: 2021-04-18
lang: fr
categories: Extérieur
tags: ["extérieur", "connaissances", "auteurs"]
---

{{% admonition quote "En écoute :" %}}[Claude Lévi-Strauss, race et histoire](https://www.franceinter.fr/emissions/intelligence-service/intelligence-service-17-avril-2021).{{% /admonition %}}

Précurseur de ce qui est nommé aujourd'hui "l'effondrement", pessimiste anthropologique, contempteur du progrès scientifique, prophète des ravages des civilisations industrielle et occidentale sur le monde, pourfendeur de l'idée d'exception et de supériorité humaines, Claude Lévi-Strauss a certainement été un des grands contributeurs à la remise en cause de ce qui faisait « l'identité » de l'Occident.

Il y a quelques décennies seulement, on ne pouvait pas penser le relativisme culturel. Il y avait des races/civilisations supérieures, et d'autres inférieures.

Il a fallu des personnes pour mener ce combat d'idées, pour finalement faire accepter que "la notion de race n'est pas scientifiquement fondée" et que l'idée de supériorité d'un groupe humain sur un autre est purement subjective (et détestable).

Hommage à Claude Lévi-Strauss, et à toutes celles et ceux qui ont pavé la voie pour que nous puissions penser la différence, sortir de l'absolutisme culturel et embrasser l'unité du genre humain.

## Citations

Une définition de la civilisation occidentale :

{{% admonition quote "[...] la connaissance scientifique et l'empire sur la nature : c'est là le caractère distinctif de la civilisation occidentale." %}}{{% /admonition %}}

La projection subjective :

{{% admonition quote "[...] chaque fois que des gens pensent avoir découvert les nécessités fondamentales de tous les êtres humains, ils ont simplement projeté les leurs en fait." %}}{{% /admonition %}}

Sur le vivre ensemble :

{{% admonition quote "Les différences raciales ne continueraient-elles pas à servir de prétexte à la difficulté croissante de vivre ensemble, inconsciemment ressentie par une humanité en proie à l'explosion démographique, et qui se mettrait à se haïr elle-même parce qu'une pression secrète l'avertit qu'elle devient trop nombreuse pour que chacun de ses membres puisse librement jouir de ces biens essentiels que sont l'espace libre, l'eau pure, l'air non pollué."  %}}{{% /admonition %}}

22 mars 1971... Cela fait réfléchir. Cette dernière citation est à méditer, particulièrement dans le contexte des événements de ces dernières décennies.
