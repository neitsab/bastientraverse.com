---
title: ffmpeg set sub as default
date: 2015-05-01T14:34:00+02:00
published: 2022-03-13
lang: en
categories: Tech
tags: ["tech", "media", "ffmpeg", "archive"]
#slug: ffmpeg-default-stream
---

Copy of my [answer](https://superuser.com/q/700082/393261) to the question "[Is there an option in ffmpeg to specify a subtitle track that should be shown by default?](https://superuser.com/questions/700082/is-there-an-option-in-ffmpeg-to-specify-a-subtitle-track-that-should-be-shown-by)" on superuser:

> ## No, there isn't an option in ffmpeg to specify a subtitle track that should be shown by default.
>
> (ffmpeg version 2.6.1 built with gcc 4.9.2 (GCC) 20150304 (prerelease) as default on Arch, see end of answer for compile flags)
>
> Resources highlighting the lack of command line options for setting default/forced subtitles stream in ffmpeg include the following:
>
> - [this Oct. 2012 mailing list thread](https://ffmpeg.org/pipermail/ffmpeg-user/2012-October/010769.html) (which delves in the matroska format and explains that ffmpeg doesn't write in the required fields)
> - [this Jan. 2013 thread](https://ffmpeg.org/pipermail/ffmpeg-user/2013-January/012997.html) (shows the lack of interest of ffmpeg devs for this)
> - [this Jan. 2014 thread](https://ffmpeg.org/pipermail/ffmpeg-user/2014-January/019641.html) (asks about flipping the default flag in Matroska container, to no avail)
> - [this very recent forum post](https://ffmpeg.zeranoe.com/forum/viewtopic.php?t=2254) (MP4, zeranoe ffmeg forums)
>
> Finally, [this question](https://stackoverflow.com/questions/26956762/ffmpeg-set-subtitles-track-as-default) on StackOverflow asks the same as you but for mkv, and highlights the fact that ffmpeg is the culprit since [mkvpropedit](https://www.bunkus.org/videotools/mkvtoolnix/doc/mkvpropedit.html) (part of *mkvtoolnix*) can do it.
>
> ### However, there is a potential workaround
>
> [This March 2013 ffmpeg bug report](https://trac.ffmpeg.org/ticket/2402)  (still open) deals with mkv and default flag for subtitles and explains something that we can use to circumvent this limitation in some scenarios:
>
> The Matroska specification - ​http://matroska.org/technical/specs/index.html (search for FlagDefault) - specifies that the value for the default flag is "default" if nothing is specified, the demuxer therefore marks all subtitle tracks as "default".
>
> So ffmpeg *may* write the default flag on our subtitles stream in some cases; I tried it with different input video files and got mixed results:
>
> - with the [Big Buck Bunny](https://peach.blender.org/download/) AVI Mpeg4  and a random .srt file, the remuxed mkv subtitles stream **did** have the default flag:
>
>         $ ffmpeg -i big_buck_bunny_480p_surround-fix.avi -i subtitle.srt -c copy test.mkv
>         $ ffmpeg -i test.mkv
>         Input #0, matroska,webm, from 'test.mkv':
>           Metadata:
>             ENCODER         : Lavf56.25.101
>           Duration: 00:28:31.40, start: 0.000000, bitrate: 1028 kb/s
>             Stream #0:0: Video: mpeg4 (Simple Profile), yuv420p, 854x480 [SAR 1:1 DAR 427:240], 24 fps, 24 tbr, 1k tbn, 24 tbc (default)
>             Stream #0:1: Audio: ac3, 48000 Hz, 5.1(side), fltp, 448 kb/s (default)
>             Stream #0:2: Subtitle: subrip (default)
>
> - however I couldn't reproduce this with a [sample mp4-contained H264 sample](http://techslides.com/sample-webm-ogg-and-mp4-video-files-for-html5) neither with a personal mp4, so I guess `mp4 --> mkv` doesn't work. But what if...?
>
>         $ ffmpeg -i sample.mp4 -c copy sample.avi
>         $ ffmpeg -i sample.avi -i subtitle.srt -c copy sample.mkv
>         $ ffmpeg -i sample.mkv
>         ...
> 		Input #0, matroska,webm, from 'sample.mkv':
> 		  Metadata:
> 			ENCODER         : Lavf56.25.101
> 		  Duration: 00:28:31.40, start: 0.000000, bitrate: 2 kb/s
> 			Stream #0:0: Video: h264 (Constrained Baseline), yuv420p(tv, bt709), 560x320, SAR 1:1 DAR 7:4, 60 fps, 60 tbr, 1k tbn, 60 tbc (default)
> 			Stream #0:1: Audio: aac (LC), 48000 Hz, mono, fltp (default)
> 			Stream #0:2: Subtitle: subrip (default)
>
> **VICTORY!** Doing `mp4 --> avi` and then `avi + srt --> mkv` gets the subtitles stream on by default and VLC displays them correctly.
>
> Sure that's very roundabout and I'd rather not think of the information loss in the process, but at least it works.
>
> ### Summary
>
> * ffmpeg is definitely not adapted right now for setting streams as default or forced,
> * mkv is probably better than mp4 as an origin container, since it has a known tool doing the job fine ([mkvpropedit](https://www.bunkus.org/videotools/mkvtoolnix/doc/mkvpropedit.html) from *mkvtoolnix*).
>
>
> #### ffmpeg version details:
>
> `ffmpeg version 2.6.1 built with gcc 4.9.2 (GCC) 20150304 (prerelease)
>   configuration: --prefix=/usr --disable-debug --disable-static --disable-stripping --enable-avisynth --enable-avresample --enable-fontconfig --enable-gnutls --enable-gpl --enable-libass --enable-libbluray --enable-libfreetype --enable-libfribidi --enable-libgsm --enable-libmodplug --enable-libmp3lame --enable-libopencore_amrnb --enable-libopencore_amrwb --enable-libopenjpeg --enable-libopus --enable-libpulse --enable-libschroedinger --enable-libspeex --enable-libssh --enable-libtheora --enable-libv4l2 --enable-libvorbis --enable-libvpx --enable-libx264 --enable-libx265 --enable-libxvid --enable-runtime-cpudetect --enable-shared --enable-swresample --enable-vdpau --enable-version3 --enable-x11grab`
