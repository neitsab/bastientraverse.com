---
title: Editor-induced bugs
created: 2021-05-02T01:51:02+02:00
updated: 2024-02-08T11:39:09+05:30
pubdate: 2024-02-08
lang: en
categories: Tech
tags: [ "tech", "bug", "ssg", "hugo", "gnome", "site"]
#slug: "editor-induced-bugs"
---

While working on this site, I suddenly noticed that an extra space was added after any link, but it was only visible when that link was followed by another character like a parenthesis.

I dug hard and on a hunch, found that it was caused by my [recent addition of a render hook for links](https://gitlab.com/neitsab/bastientraverse.com/-/commit/387534693548bdf0e61d62df12f723ac98167f8c). I had simply [followed the instructions](https://gohugo.io/templates/render-hooks/#link-with-title-markdown-example) and added the following snippet in `layouts/_default/_markup/render-link.html`:

```go
<a href="{{ .Destination | safeURL }}"{{ with .Title}} title="{{ . }}"{{ end }}{{ if strings.HasPrefix .Destination "http" }} target="_blank" rel="noopener"{{ end }}>{{ .Text | safeHTML }}</a>
```

Eventually, I found this [Hugo bug](https://github.com/gohugoio/hugo/issues/6949) that enlightened me: turns out some text editors automatically add a newline at the end of the file on save to comply with the [POSIX standard definition of a line ](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_206)([source](https://stackoverflow.com/questions/729692/why-should-text-files-end-with-a-newline)).

This other [StackOverflow thread](https://stackoverflow.com/questions/3056740/gedit-adds-line-at-end-of-file) lists [some](https://web.archive.org/web/20230530154654/https://bugzilla.gnome.org/show_bug.cgi?id=625955) Gedit [bugs](https://bugzilla.gnome.org/show_bug.cgi?id=526612) where devs don't want to add at least a GUI option to enable/disable the automatic addition of newline (the option now exists in gsettings), nor to simply display it... Some more discussion of this issue on [Hugo Forums](https://discourse.gohugo.io/t/markdown-render-hooks-extra-space/27446).

The Hugo-specific solution was to add a Go template snippet at the end of the render hooks which removes trailing newlines ([credit](https://discourse.gohugo.io/t/markdown-render-hooks-extra-space/27446/5)):

```diff
diff --git a/layouts/_default/_markup/render-link.html b/layouts/_default/_markup/render-link.html
index f04b2e3bda072e15cf8ac7787e37e68744a76048..06727fbe334a084ad29a29a85caf8f3a4fd55bda 100644
--- a/layouts/_default/_markup/render-link.html
+++ b/layouts/_default/_markup/render-link.html
@@ -1 +1,2 @@
 <a href="{{ .Destination | safeURL }}"{{ with .Title}} title="{{ . }}"{{ end }}{{ if strings.HasPrefix .Destination "http" }} target="_blank" rel="noopener"{{ end }}>{{ .Text | safeHTML }}</a>
+{{- /* This comment removes trailing newlines. */ -}}
```

Information technology is such a complex interaction of countless moving parts, that I often wonder how it can work and produce any results at all!
