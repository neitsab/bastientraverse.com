---
title: Hugo on Vercel
created: 2021-04-26
lastmod: 2021-07-14
published: 2021-07-14
categories: Tech
lang: en
tags:
  - tech
  - web
  - blog
  - hugo
---

As an attempt to diversify, I have been using [Vercel](https://vercel.com/) to build and host this website since its inception, as opposed to going with the more common choice of [Netlify](https://www.netlify.com/).

After being pleasantly surprised with the initial setup-and-first-build flow (the automatic HTTPS on custom domains is awesome), I stumbled upon something that baffled me: **the default Hugo version Vercel uses to build your site dates from September 2019**, and you have to jump through hoops to find out about it.

## Getting the version of Hugo used to build your site

This came out due to the many discrepancies I was seeing between my local dev version (using the latest and shiniest `v0.82.1`) and the deployed site. It took me a couple of weeks before I realized that Vercel was not using the latest stable version as I was naïvely thinking, but was in fact using version... `0.58.2` from **2019-09-13**!! 😨

In order to get this basic information, you need to [change the build command](https://github.com/vercel/vercel/discussions/5834#discussioncomment-362986) to something like:

    hugo version && hugo -D --gc

I'd hope Vercel would make this the default build command, as this undocumented<sup>[[1](https://vercel.com/guides/deploying-hugo-with-vercel?query=hugo#optional:-define-a-hugo-version)][[2](https://vercel.com/docs/build-step#framework-versioning)]</sup> peculiarity made my first blog-and-deploy experience far more annoying than it ought to be.

I really don't understand why they'd use such an outdated version, without even so much as a warning. Quite some things happened in Hugo-land since the somewhat "dramatic" [changes in v0.60](https://gohugo.io/news/0.60.0-relnotes/), which may have prompted this version pinning.

{{% admonition tip Tip %}}If someone at Vercel reads this, please either use the latest version by default or include the one you are using in your Docs and offer a way to specify a "latest" as the `HUGO_VERSION` environment variable.{{% /admonition %}}
